{
  "name": "Helm",
  "description": "GitLab CI template for [Helm](https://helm.sh/)",
  "template_path": "templates/gitlab-ci-helm.yml",
  "kind": "hosting",
  "variables": [
    {
      "name": "HELM_CLI_IMAGE",
      "description": "The Docker image used to run Helm - **set the version required by your Kubernetes server**",
      "default": "alpine/helm:latest"
    },
    {
      "name": "HELM_CHART_DIR",
      "description": "The folder in which is stored the Helm chart",
      "default": "."
    },
    {
      "name": "HELM_DEPLOY_CHART",
      "description": "The Helm [chart](https://helm.sh/docs/topics/charts/) to deploy. _Only required if you want to deploy an **external** chart._"
    },
    {
      "name": "HELM_REPOS",
      "description": "The Helm [chart repositories](https://helm.sh/docs/topics/chart_repository/) to use (formatted as `repo_name_1@:repo_url_1 repo_name_2@:repo_url_2 ...`)",
      "default": "stable@https://charts.helm.sh/stable bitnami@https://charts.bitnami.com/bitnami"
    },
    {
      "name": "KUBE_NAMESPACE",
      "description": "The default Kubernetes namespace to use. _Leave default if [GitLab Kubernetes integration](https://docs.gitlab.com/ee/user/project/clusters/index.html) is enabled._"
    },
    {
      "name": "HELM_DEFAULT_KUBE_CONFIG",
      "description": "The default kubeconfig content to use. Automatically set if GitLab Kubernetes integration is enabled.",
      "default": "$KUBECONFIG",
      "secret": true
    },
    {
      "name": "HELM_BASE_APP_NAME",
      "description": "Base application name",
      "default": "$CI_PROJECT_NAME",
      "advanced": true
    },
    {
      "name": "HELM_DEPLOY_ARGS",
      "description": "The Helm [command with options](https://helm.sh/docs/helm/helm_upgrade/) to deploy the application (_without dynamic arguments such as release name and chart_)",
      "default": "upgrade --install --atomic --timeout 120s",
      "advanced": true
    },
    {
      "name": "HELM_DELETE_ARGS",
      "description": "The Helm [command with options](https://helm.sh/docs/helm/helm_uninstall/) to cleanup the application (_without dynamic arguments such as release name_)",
      "default": "uninstall",
      "advanced": true
    },
    {
      "name": "HELM_DEPENDENCY_ARGS",
      "description": "The Helm [command with options](https://helm.sh/docs/helm/helm_dependency_update/) to update on-disk the chart dependencies (_without dynamic arguments such as the chart path_)",
      "default": "dependency update",
      "advanced": true
    },
    {
      "name": "HELM_ENV_VALUE_NAME",
      "description": "The environment type variable set to helm",
      "default": "env",
      "advanced": true
    },
    {
      "name": "HELM_HOSTNAME_VALUE_NAME",
      "description": "The hostname variable set to helm",
      "default": "hostname",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "lint",
      "name": "Helm Lint",
      "description": "Static code analysis of your Helm chart with [Helm Lint](https://helm.sh/docs/helm/helm_lint/)",
      "disable_with": "HELM_LINT_DISABLED",
      "variables": [
        {
          "name": "HELM_LINT_ARGS",
          "description": "The Helm [command with options](https://helm.sh/docs/helm/helm_lint/) to trigger the analysis (_without dynamic arguments such as the chart path_)",
          "default": "lint --strict",
          "advanced": true
        }
      ]
    },
    {
      "id": "test",
      "name": "Helm Test",
      "description": "Acceptance test of your Helm chart with [Helm Test](https://helm.sh/docs/helm/helm_test/)",
      "enable_with": "HELM_TEST_ENABLED",
      "variables": [
        {
          "name": "HELM_TEST_ARGS",
          "description": "The Helm [command with options](https://helm.sh/docs/helm/helm_test/) to perform acceptance test (_without dynamic arguments such as release name_)",
          "default": "test",
          "advanced": true
        }
      ]
    },
    {
      "id": "yamllint",
      "name": "Yaml Lint",
      "description": "[Yaml Lint](https://github.com/adrienverge/yamllint) of your Helm [values file](https://helm.sh/docs/chart_template_guide/values_files/)",
      "disable_with": "HELM_YAMLLINT_DISABLED",
      "variables": [
        {
          "name": "HELM_YAMLLINT_IMAGE",
          "description": "The Docker image used to run YamlLint test",
          "default": "cytopia/yamllint"
        },
        {
          "name": "HELM_YAMLLINT_CONFIG",
          "description": "Config used with the yamllint tool",
          "default": "{ extends: relaxed, rules: { line-length: { max: 160 } } }",
          "advanced": true
        },
        {
          "name": "HELM_YAMLLINT_ARGS",
          "description": "Arguments used by the lint job",
          "default": "-f colored --strict",
          "advanced": true
        }
      ]
    },
    {
      "id": "kube-score",
      "name": "Kube-Score",
      "description": "Run [Kube-Score](https://kube-score.com/) on the resources to be created by Helm",
      "disable_with": "HELM_KUBE_SCORE_DISABLED",
      "variables": [
        {
          "name": "HELM_KUBE_SCORE_IMAGE",
          "description": "The Docker image used to run [Kube-Score](https://kube-score.com/)",
          "default": "zegl/kube-score:latest-helm3"
        },
        {
          "name": "HELM_KUBE_SCORE_ARGS",
          "description": "Arguments used by the kube-score job",
          "advanced": true
        }
      ]
    },
    {
      "id": "package",
      "name": "Package your chart",
      "description": "[Package](https://helm.sh/docs/helm/helm_package/) your Helm chart",
      "variables": [
        {
          "name": "HELM_PACKAGE_ARGS",
          "description": "The Helm [command with options](https://helm.sh/docs/helm/helm_package/) to perform the packaging (_without dynamic arguments such as the chart path_)",
          "default": "package --dependency-update",
          "advanced": true
        },
        {
          "name": "HELM_SEMREL_RELEASE_DISABLED",
          "description": "Disable semantic-release integration",
          "advanced": true
        }
      ]
    },
    {
      "id": "publish",
      "name": "Publish your chart",
      "description": "Publish your Helm chart",
      "variables": [
        {
          "name": "HELM_PUBLISH_ARGS",
          "description": "Arguments used by the Helm publish job",
          "advanced": true
        },
        {
          "name": "HELM_PUBLISH_DIR",
          "description": "The folder in which the job will publish the chart",
          "default": ".",
          "advanced": true
        },
        {
          "name": "HELM_PUBLISH_URL",
          "description": "The URL of the the chart to publish",
          "mandatory": true
        }
      ]
    },
    {
      "id": "review",
      "name": "Review",
      "description": "Dynamic review environments for your topic branches (see GitLab [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/))",
      "disable_with": "HELM_REVIEW_DISABLED",
      "variables": [
        {
          "name": "HELM_REVIEW_APP_NAME",
          "description": "The application name for `review` env _(only define to override default)_",
          "default": "${HELM_BASE_APP_NAME}-${CI_ENVIRONMENT_SLUG}",
          "advanced": true
        },
        {
          "name": "HELM_REVIEW_VALUES",
          "description": "The Values file to use with `review` environment"
        },
        {
          "name": "HELM_REVIEW_NAMESPACE",
          "description": "The Kubernetes namespace to use for `review` env _(only define to override default)_",
          "default": "$KUBE_NAMESPACE",
          "advanced": true
        },
        {
          "name": "HELM_REVIEW_KUBE_CONFIG",
          "description": "kubeconfig content used for review env (only define to override default)",
          "default": "$HELM_DEFAULT_KUBE_CONFIG",
          "advanced": true,
          "secret": true
        }
      ]
    },
    {
      "id": "integration",
      "name": "Integration",
      "description": "A continuous-integration environment associated to your integration branch (`develop` by default)",
      "disable_with": "HELM_INTEG_DISABLED",
      "variables": [
        {
          "name": "HELM_INTEG_APP_NAME",
          "description": "The application name for `integration` env _(only define to override default)_",
          "default": "${HELM_BASE_APP_NAME}-integration",
          "advanced": true
        },
        {
          "name": "HELM_INTEG_VALUES",
          "description": "The Values file to use with `integration` environment"
        },
        {
          "name": "HELM_INTEG_NAMESPACE",
          "description": "The Kubernetes namespace to use for `integration` env _(only define to override default)_",
          "default": "$KUBE_NAMESPACE",
          "advanced": true
        },
        {
          "name": "HELM_INTEG_KUBE_CONFIG",
          "description": "kubeconfig content used for integration env (only define to override default)",
          "default": "$HELM_DEFAULT_KUBE_CONFIG",
          "advanced": true,
          "secret": true
        }
      ]
    },
    {
      "id": "staging",
      "name": "Staging",
      "description": "An iso-prod environment meant for testing and validation purpose on your production branch (`master` by default)",
      "disable_with": "HELM_STAGING_DISABLED",
      "variables": [
        {
          "name": "HELM_STAGING_APP_NAME",
          "description": "The application name for `staging` env _(only define to override default)_",
          "default": "${HELM_BASE_APP_NAME}-staging",
          "advanced": true
        },
        {
          "name": "HELM_STAGING_VALUES",
          "description": "The Values file to use with `staging` environment"
        },
        {
          "name": "HELM_STAGING_NAMESPACE",
          "description": "The Kubernetes namespace to use for `staging` env _(only define to override default)_",
          "default": "$KUBE_NAMESPACE",
          "advanced": true
        },
        {
          "name": "HELM_STAGING_KUBE_CONFIG",
          "description": "kubeconfig content used for staging env (only define to override default)",
          "default": "$HELM_DEFAULT_KUBE_CONFIG",
          "advanced": true,
          "secret": true
        }
      ]
    },
    {
      "id": "prod",
      "name": "Production",
      "description": "The production environment",
      "disable_with": "HELM_PROD_DISABLED",
      "variables": [
        {
          "name": "AUTODEPLOY_TO_PROD",
          "type": "boolean",
          "description": "Set this variable to auto-deploy to production. If not set deployment to production will be manual (default behaviour)."
        },
        {
          "name": "HELM_PROD_APP_NAME",
          "description": "The application name for `production` env _(only define to override default)_",
          "default": "${HELM_BASE_APP_NAME}",
          "advanced": true
        },
        {
          "name": "HELM_PROD_VALUES",
          "description": "The Values file to use with `production` environment"
        },
        {
          "name": "HELM_PROD_NAMESPACE",
          "description": "The Kubernetes namespace to use for `production` env _(only define to override default)_",
          "default": "$KUBE_NAMESPACE",
          "advanced": true
        },
        {
          "name": "HELM_PROD_KUBE_CONFIG",
          "description": "kubeconfig content used for production env (only define to override default)",
          "default": "$HELM_DEFAULT_KUBE_CONFIG",
          "advanced": true,
          "secret": true
        }
      ]
    }
  ],
  "variants": [
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-helm-vault.yml",
      "variables": [
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url"
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    }
  ]
}
